package boot.spring.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import boot.spring.pagemodel.ActorGrid;
import boot.spring.po.Actor;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "jdbcTemplate接口")
@Controller
public class JdbcTemplateController {
	
	@Autowired
	@Qualifier("mysqlTemplate")
	JdbcTemplate mysqlTemplate;
	
	@Autowired
	@Qualifier("pgTemplate")
	JdbcTemplate pgTemplate;
	
	@Autowired
	@Qualifier("oracleTemplate")
	JdbcTemplate oracleTemplate;
	
	@ApiOperation("查询mysql")
	@RequestMapping(value="/jdbc/actors/{id}/{name}",method = RequestMethod.GET)
	@ResponseBody
	public List<Actor> getactorlist(@PathVariable("id")Short id,@PathVariable("name")String name){
		List<Actor> list = mysqlTemplate.query("select * from actor where actor_id = ? or first_name = ?",new Object[]{id,name}, new BeanPropertyRowMapper<>(Actor.class));
		return list;
	}
	
	@ApiOperation("查询potgresql")
	@RequestMapping(value="/jdbc/hotwords/{word}",method = RequestMethod.GET)
	@ResponseBody
	public List<Map<String, Object>> gethotwords(@PathVariable("word")String word){
		List<Map<String, Object>> list = pgTemplate.queryForList("select * from hotwords where hotword = ?", new Object[]{word});
		return list;
	}	

	@ApiOperation("查询oracle")
	@RequestMapping(value="/jdbc/dw",method = RequestMethod.GET)
	@ResponseBody
	public List<Map<String, Object>> gethotwords(){
		List<Map<String, Object>> list = oracleTemplate.queryForList("select * from HBHZK.dwb " );
		return list;
	}	
}
